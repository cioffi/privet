package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
	"os"
	//"net/http"
	"net/url"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/sheets/v4"
)

const googleCredsJSON = `{"installed":{"client_id":"165693858535-01uqk8uc9c2uusv2cfp6td2skpeit7i5.apps.googleusercontent.com","project_id":"privet-1604147277961","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://oauth2.googleapis.com/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","client_secret":"pJrk3eUlEzK1qcMsIyMVSxjd","redirect_uris":["urn:ietf:wg:oauth:2.0:oob","http://localhost"]}}`

// client id:  165693858535-01uqk8uc9c2uusv2cfp6td2skpeit7i5.apps.googleusercontent.com
// sekrit:  pJrk3eUlEzK1qcMsIyMVSxjd
func fatal(err error, msg string) {
	log.Fatal().
		Err(err).
		Msg(msg)
}

func do_sheets() {
	cfg, err := google.ConfigFromJSON(googleCredsJSON, "https://www.googleapis.com/auth/spreadsheets")
	if err != nil {
		fatal(err, "google.ConfigFromJSON")
	}

	dir := os.Getenv("XDG_CONFIG_HOME")
	if dir == "" {
		dir, err = os.UserHomeDir()
		if err != nil {
			fatal(err, "os.UserHomeDir")
		}
		dir += "/.privet"
	} else {
		dir += "/privet/.token"
	}

	var authCode string
	f, err := os.Open(dir)
	if err != nil {
		authURL := cfg.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
		fmt.Printf("Go to the following link in your browser then type the "+
				"authorization code: \n%v\n", authURL)

		if _, err := fmt.Scan(&authCode); err != nil {
			log.Fatal().
				Err(err).
				Msg("Unable to read authorization code")
		}

		f, err = os.Create(dir)
		if err != nil {
			fatal(err, "os.create token file")
		}

		f.WriteString(authCode)
		f.Close()
	} else {
		defer f.Close()
		authCode = ioutil.ReadAll(f)
	}

	tok, err := cfg.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Unable to retrieve token from web")
	}

	client := cfg.Client(context.Background(), tok)

	svr, err := sheets.New(client)
	_ = svr.Spreadsheets
}

type Parser interface {
	Parse(data []byte) (*RealEstate, error)
}

type ParserGenerator func() Parser

type Acre float32

type EstateType byte

type SourceType byte

type Address struct {
	Number int
	Street string
	City string
	State string
	Zip int
}

type RealEstate struct {
	Source url.URL
	Type EstateType
	Address Address
	LotSize Acre
}

type ZillowParser struct {
}

func NewZillowParser() Parser {
	return &ZillowParser{};
}

func (p *ZillowParser) Parse([]byte) (*RealEstate, error) {
	return nil, nil
}

type RedfinParser struct {
}

func NewRedfinParser() Parser {
	return &RedfinParser{};
}

func (p *RedfinParser) Parse([]byte) (*RealEstate, error) {
	return nil, nil
}


const (
	ETUnknown = EstateType(iota+1)
	ETSingle
	ETMulti
	ETCondo
	ETLand
)

const zillowTitleIdentifier = "Zillow"
const redfinTitleIdentifier = "Redfin"

var sourceIdentifierToParser = map[string]ParserGenerator {
	zillowTitleIdentifier: NewZillowParser,
	redfinTitleIdentifier: NewRedfinParser,
}

var combinedIdents = strings.Join([]string{zillowTitleIdentifier, redfinTitleIdentifier}, "|")
var reIdentifier = regexp.MustCompile("<title>.*?(" + combinedIdents + ").*?</title>")

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: ./pvt_scraper")
		return
	}

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnixMs
	zerolog.SetGlobalLevel(zerolog.DebugLevel)

	do_sheets()
/*
	target := "https://www.zillow.com/homedetails/5-Lawrence-Rd-Salem-NH-03079/2081414209_zpid/";
	req, err := http.NewRequest("GET", target, nil)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Error creating http request")
	}

	req.Header.Set("User-Agent", "MyFirstWebBrowser")

	cli := http.Client{}
	rsp, err := cli.Do(req)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Error doing.")
	}
	defer rsp.Body.Close()
	d, e := ioutil.ReadAll(rsp.Body)
	log.Info().
		Err(e).
		Str("body", string(d)).
		Msg("Content")
/*
	data, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		log.Fatal().
			Err(err).
			Str("filename", os.Args[1]).
			Msg("Reading file failed.")
	}
*/

	//src := identifySource(data)
	//result := parsers[src].Parse(src)
}
